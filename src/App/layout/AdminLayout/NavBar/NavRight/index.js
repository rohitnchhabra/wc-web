import React, { Component } from 'react';
import {
    Dropdown,
    Button,
    Form
} from 'react-bootstrap';
import PerfectScrollbar from 'react-perfect-scrollbar';
import { connect } from "react-redux";

import Aux from "../../../../../hoc/_Aux";
import DEMO from "../../../../../store/constant";

import * as constants from '../../../../../constants'

import Select from 'react-select';

class NavRight extends Component {

    render() {
        return (
            <Aux>
                <ul className="navbar-nav ml-auto">
                    <li>
                        <Dropdown alignRight={!this.props.rtlLayout} className="drp-user">
                            <Dropdown.Toggle variant={'link'} id="dropdown-basic">
                                <i className="icon feather icon-user" />
                            </Dropdown.Toggle>
                            <Dropdown.Menu alignRight className="profile-notification">
                                <div className="pro-head">
                                    {/* <img src={Avatar1} className="img-radius" alt="User Profile"/> */}
                                    <span>TEST</span>
                                    <a href={DEMO.BLANK_LINK} className="dud-logout" title="Logout" onClick={() => { }}>
                                        <i className="feather icon-log-out" />
                                    </a>
                                </div>
                                <ul className="pro-body">

                                    <li><a href={constants.routeProfile} className="dropdown-item"><i className="feather icon-user" /> Profile</a></li>

                                    <li><a href={DEMO.BLANK_LINK} className="dropdown-item" onClick={() => { }}><i className="feather icon-lock" /> Logout</a></li>
                                </ul>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>
                </ul>
            </Aux>
        );
    }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps)(NavRight);
//export default NavRight;
