import React from 'react';
import {
    Form,
} from 'react-bootstrap';

export default function FormInput({
    id, labelName, value, handleChange, handleBlur, errors, touched, disabled, type, defaultValue,
    displayRequired
}) {

    var validationError, input;

    if(errors && touched) {
        validationError = <div className="form-input-invalid-feedback">{errors}</div>
    }

    if(type == "textarea") {
        input = (
            <textarea
                disabled={disabled}
                name={id}
                id={id}
                className="form-control border rounded padding-horizontal" 
                placeholder=""
                value={value}
                onChange={handleChange}
                onBlur={handleBlur}
            >
            </textarea>
        )
    } else {
        input = (
            <input   
                type={(type) ? type: 'text'}
                disabled={disabled}
                name={id}
                id={id}
                className="form-control border rounded-pill padding-horizontal" 
                placeholder=""
                defaultValue={defaultValue}
                value={value}
                onChange={handleChange}
                onBlur={handleBlur}
            />
        )
        
    }

    return (
        <div>
            <Form.Label htmlFor={id} className="font-weight-bolder">
                {labelName}
                {displayRequired && <span style={{color: 'red'}}> *</span>}
            </Form.Label>
            {input}
            {validationError}
        </div>
    );
}