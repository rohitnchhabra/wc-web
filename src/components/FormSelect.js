import React from "react"
import {
    Form,
    Col
} from 'react-bootstrap';
import Select from 'react-select';

export default function FormSelect({size, label, placeholder, options, defaultValue, onChange, errors, touched}){ 
        
        return (
            <Form.Group as={Col} sm={size}>
                <Form.Label htmlFor={label} className="font-weight-bolder">{label}</Form.Label>
                {
                    defaultValue &&
                    <Select
                        closeMenuOnSelect={true}
                        placeholder={placeholder}
                        options={options}
                        defaultValue={defaultValue}
                        onChange={onChange}
                    />
                }

                {
                    defaultValue == null &&
                    <Select
                        closeMenuOnSelect={true}
                        placeholder={placeholder}
                        options={options}
                        defaultValue={defaultValue}
                        onChange={onChange}
                    />
                }

                {errors && touched && (
                    <div className="form-input-invalid-feedback">{errors}</div>
                )}

            </Form.Group>
        )
    
    
    

}