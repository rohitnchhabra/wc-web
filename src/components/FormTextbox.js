import React from 'react';
import {
    Form,
    Col
} from 'react-bootstrap';

export default function FormTextbox({
    id, labelName, value, size, type, placeholder, minValue, maxValue, stepValue, handleChange, handleBlur, errors, touched
}) {

    //const min = minValue ?? ''

    var validationError;

    if(errors && touched) {
        validationError = <div className="form-input-invalid-feedback">{errors}</div>
    }

    return (
        <Form.Group as={Col} sm={size}>
            <Form.Label htmlFor={id} className="font-weight-bolder">{labelName}</Form.Label>
            <input 
                type={type} 
                name={id}
                id={id}
                className="form-control border rounded-pill padding-horizontal" 
                placeholder={placeholder}
                value={value}
                onChange={handleChange}
                onBlur={handleBlur}
                min={minValue}
                max={maxValue}
                step={stepValue}
            />
            {validationError}
        </Form.Group>
    );
}