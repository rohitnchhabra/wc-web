import React from 'react';
import {Line} from 'react-chartjs-2';

class LineBasicChart extends React.Component {

    render() {

        const {
            labels,
            legendLabel1,
            legendLabel2,
            lineBasicChartData1,
            lineBasicChartData2
        } = this.props

        const data = (canvas) => {
            let bar = canvas.getContext('2d');
            let theme_g1 = bar.createLinearGradient(0, 0, 500, 0);
            theme_g1.addColorStop(0, '#4680ff');
            theme_g1.addColorStop(1, '#4680ff');
            let theme_g2 = bar.createLinearGradient(0, 0, 500, 0);
            theme_g2.addColorStop(0, '#11c15b');
            theme_g2.addColorStop(1, '#11c15b');

            return {
                labels: labels,
                datasets: [{
                    label: legendLabel1,
                    data: lineBasicChartData1,
                    fill: true,
                    borderWidth: 4,
                    borderColor: theme_g1,
                    backgroundColor: theme_g1,
                    hoverborderColor: theme_g1,
                    hoverBackgroundColor: theme_g1,
                }, {
                    label: legendLabel2,
                    data: lineBasicChartData2,
                    fill: true,
                    borderWidth: 4,
                    borderColor: theme_g2,
                    backgroundColor: theme_g2,
                    hoverborderColor: theme_g2,
                    hoverBackgroundColor: theme_g2,
                },]
            }
        };

        return (
            <Line
                data={data}
                responsive={true}
                height={300}
                options={{
                    barValueSpacing: 20,
                    maintainAspectRatio: false,
                }}
            />
        );
    }
}

export default LineBasicChart;