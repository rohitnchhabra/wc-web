import React from 'react';
import {Line} from 'react-chartjs-2';

class LineOriginChart extends React.Component {

    render() {

        const {
            legendLabel,
            labels,
            lineOriginChartData,
        } = this.props

        const data = (canvas) => {
            let bar = canvas.getContext('2d');
            let theme_g1 = bar.createLinearGradient(0, 0, 500, 0);
            theme_g1.addColorStop(0, '#4680ff');
            theme_g1.addColorStop(1, '#4680ff');

            return {
                labels: labels,
                datasets: [{
                    label: legendLabel,
                    data: lineOriginChartData,
                    borderWidth: 1,
                    borderColor: theme_g1,
                    backgroundColor: theme_g1,
                    hoverborderColor: theme_g1,
                    hoverBackgroundColor: theme_g1,
                    fill: 'origin',
                }]
            }
        };

        return (
            <Line
                data={data}
                responsive={true}
                height={300}
                options={{
                    barValueSpacing: 20,
                    maintainAspectRatio: false,
                }}
            />
        );
    }
}

export default LineOriginChart;