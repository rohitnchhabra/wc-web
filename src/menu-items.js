
export default function getMenu() {
    return [
        {
            id: 'support',
            title: 'Support',
            type: 'group',
            icon: 'icon-support',
            children: [
                {
                    id: 'dashboard',
                    title: 'Tab 1',
                    type: 'item',
                    icon: 'fas fa-chart-line',
                    url: '/consult',
                },
            ]
        }
    ]
}