import React, { useState } from "react";
import {
  Row,
  Col,
  Card,
  Modal,
  Button,
  Table,
  Form,
  Tabs,
  Tab,
  Container,
  Nav,
  InputGroup,
  Collapse,
} from "react-bootstrap";
import { connect } from "react-redux";
import FormInput from "../../../components/FormInput";

import Aux from "../../../hoc/_Aux";
import DATA from "../../../assets/json/sample.json";
import TreeGraph from "react-d3-tree";

import cloneDeep from "clone-deep";
// import Input from "react-select/src/components/Input";
import "./TestPage.scss";

const treetraverseDF = function (callback, data) {
  (function recurse(currentNode) {
    if (currentNode.children) {
      for (var i = 0, length = currentNode.children.length; i < length; i++) {
        recurse(currentNode.children[i]);
      }
    }
    callback(currentNode);
  })(data);
};

const treecontains = function (callback, data, traversal) {
  traversal.call(this, callback, data);
};

const treeadd = function (allData, data, traversal) {
  var parent = null,
    callback = function (node) {
      if (node._id === data.parent_id) {
        parent = node;
      }
    };

  treecontains(callback, allData, traversal);

  if (parent) {
    if (parent.children) {
      const found = parent.children.find((v) => v._id === data._id);
      if (!found) {
        parent.children.push(data);
      }
    } else {
      parent.children = [data];
    }
  } else {
    console.log("Cannot add node to a non-existent parent.");
  }
};

const treeremove = function (allData, data, traversal) {
  var parent = null,
    childToRemove = null,
    index;

  var callback = function (node) {
    if (node._id === data.parent_id) {
      parent = node;
    }
  };

  treecontains(callback, allData, traversal);
  if (parent) {
    index = parent.children.findIndex((v) => v._id === data.id);

    if (index === undefined) {
      console.log("Node to remove does not exist.");
    } else {
      childToRemove = parent.children.splice(index, 1);
    }
  } else {
    console.log("Parent does not exist.");
  }
  return allData;
};

class NodeLabel extends React.PureComponent {
  render() {
    const { className, nodeData } = this.props;
    return (
      <div
        className={className + " btn font-weight-bold"}
        style={{
          wordBreak: "break-word",
          border: "1px solid " + nodeData.color,
          borderRadius: 10,
          background: nodeData.color,
        }}
      >
        {nodeData.name}
      </div>
    );
  }
}

class TestPage extends React.Component {
  //   Component

  constructor(props) {
    super(props);
    this.levelHeight = 100;
    this.state = {
      selectedparent: 0,
      tree: [],
      search_text: "",
      newHeight: [],
    };
  }

  componentDidMount() {}

  // Render

  onSearch = (e) => {
    this.setState({ search_text: e.target.value });
  };

  renderSection = () => {
    const { search_text, selectedparent } = this.state;
    const visible_nodes = DATA.filter(
      (v) =>
        v.parent_id === selectedparent &&
        (search_text ? v.label.includes(search_text) : true)
    );
    const svgSquare = {
      shape: "rect",
      shapeProps: {
        width: 50,
        height: 30,
        x: -10,
        y: -20,
        strokeWidth: 1,
        fill: "#fff",
      },
    };

    return (
      <Aux>
        <Card>
          <Card.Body>
            {/* <h5>Sub-header</h5> */}
            <div className="d-flex justify-content-between mb-3">
              <input
                type="text"
                className="mb-2 tag-search-input"
                onChange={this.onSearch}
                value={this.state.search_text}
                placeholder="Search"
              />
              <button
                className="btn font-weight-bold"
                onClick={() => {
                  this.setState({ selectedparent: 0 });
                }}
              >
                Reset
              </button>
            </div>
            <div className="d-flex flex-wrap">
              {visible_nodes.map((v, i) => {
                let contain = false;
                treecontains(
                  (node) => {
                    if (node._id === v.id) contain = true;
                  },
                  { children: this.state.tree },
                  treetraverseDF
                );

                return (
                  <Col key={v.id} md="3" className="px-1">
                    <button
                      onClick={() => {
                        let new_tree = cloneDeep(this.state.tree);
                        const new_data = {
                          ...v,
                          name: v.label,
                          _id: v.id,
                          nodeSvgShape: {
                            shapeProps: {
                              //     width: 50,
                              //     height: 30,
                              //     x: -10,
                              //     y: -25,
                              //     stroke: v.color,
                              //     strokeWidth: 1,
                              //     fill: v.color,
                            },
                          },
                        };
                        let { selectedparent: sp } = this.state;
                        if (v.parent_id === 0) {
                          const found = this.state.tree.find(
                            (treeItem) => treeItem.label === v.label
                          );
                          if (!found) {
                            new_tree.push(new_data);
                          } else {
                            new_tree = this.state.tree.filter(
                              (node) => node._id !== v.id
                            );
                            sp = 0;
                          }
                        } else {
                          if (contain) {
                            new_tree = treeremove(
                              { children: new_tree },
                              v,
                              treetraverseDF
                            ).children;
                            sp = v.parent_id;
                          } else {
                            treeadd(
                              { children: new_tree },
                              new_data,
                              treetraverseDF
                            );
                          }
                        }
                        this.setState({
                          tree: new_tree,
                          selectedparent: sp,
                          update: true,
                        });
                      }}
                      className="btn font-weight-bold w-100"
                      style={{
                        border: "1px solid " + v.color,
                        borderRadius: 10,
                        background: contain ? v.color : "#fff",
                      }}
                    >
                      {v.label}
                    </button>
                  </Col>
                );
              })}
            </div>
          </Card.Body>
        </Card>
        <h6>Some text here:</h6>
        {this.state.tree.map((onetree, i) => (
          <div
            key={"tree" + onetree.id}
            id="treeWrapper"
            style={{
              width: "100%",
              height: this.state.newHeight[i] || this.levelHeight,
            }}
          >
            <TreeGraph
              collapsible={false}
              pathFunc="step"
              translate={{
                x: 0,
                y: (this.state.newHeight[i] || this.levelHeight) / 2,
              }}
              data={onetree}
              // zoomable={false}
              onUpdate={(node) => {
                if (
                  this.state.update &&
                  this.state.selectedTree == onetree._id
                ) {
                  var levelWidth = [1];
                  var childCount = function (level, n) {
                    if (n.children && n.children.length > 0) {
                      if (levelWidth.length <= level + 1) levelWidth.push(0);

                      levelWidth[level + 1] += n.children.length;
                      n.children.forEach(function (d) {
                        childCount(level + 1, d);
                      });
                    } else {
                      if (levelWidth.length <= level + 1) levelWidth.push(1);
                    }
                  };
                  childCount(0, onetree);
                  var max = levelWidth.reduce(function (a, b) {
                    return Math.max(a, b);
                  });

                  var height = max * this.levelHeight; // 100 pixels per line

                  const newH = [...this.state.newHeight];
                  newH[i] = height;
                  this.setState({ newHeight: newH, update: false });
                }
              }}
              scaleExtent={{ min: -3, max: 3 }}
              nodeSize={{ x: 200, y: 100 }}
              separation={{ siblings: 0.8, nonSiblings: 1 }}
              nodeSvgShape={svgSquare}
              allowForeignObjects
              nodeLabelComponent={{
                render: <NodeLabel className="myLabelComponentInSvg" />,
                foreignObjectWrapper: {
                  // width: (window.innerWidth - 200) / 4,
                  x: 0,
                  y: -20,
                },
              }}
              onClick={(node) => {
                if (this.state.selectedparent !== node._id) {
                  this.setState({
                    selectedTree: onetree._id,
                    selectedparent: node._id,
                  });
                }
              }}
            />
            <hr />
          </div>
        ))}
      </Aux>
    );
  };

  render() {
    return (
      <Aux>
        <Row className="btn-page">
          <Col xl={3}>
            <Card className="task-board-left">
              <Card.Header>
                <Card.Title as="h5">Encounters</Card.Title>
              </Card.Header>
              <Card.Body></Card.Body>
            </Card>
          </Col>
          <Col xl={9}>
            <Card>
              <Card.Header>
                <div className="d-flex justify-content-between">
                  <h5 className="align-self-center">Header</h5>
                </div>
              </Card.Header>
              <Card.Body>
                <Row className="align-items-center m-l-0">
                  <Col />
                </Row>

                <Tabs defaultActiveKey="tab1" className="mb-3">
                  <Tab eventKey="tab1" title="TAB1">
                    {this.renderSection()}
                  </Tab>
                </Tabs>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Aux>
    );
  }
}

const mapStateToProps = (state) => ({});

export default connect(mapStateToProps)(TestPage);
