import React from "react"
import {
    Row,
    Col,
    Card,
    Modal,
    Button,
    Table,
    Form,
} from 'react-bootstrap';
import Aux from "../../hoc/_Aux";
import { connect } from "react-redux";

class Profile extends React.Component {

    componentDidMount() {
    }

    render() {
        return (
            <Aux>
                <Row className='btn-page'>
                    <Col sm={12}>
                        <Card>
                            <Card.Header>
                                <h5>Profile</h5>
                            </Card.Header>
                            <Card.Body>
                            </Card.Body>
                        </Card>
                    </Col>

                </Row>
            </Aux>
        )
    }
}

const mapStateToProps = state => ({
});

export default connect(mapStateToProps)(Profile);