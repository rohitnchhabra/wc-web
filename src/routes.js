import React from 'react';
import $ from 'jquery';
import * as constants from './constants';

window.jQuery = $;
window.$ = $;
global.jQuery = $;

//const OtherSamplePage = React.lazy(() => import('./Demo/Other/SamplePage'));

const TestPage = React.lazy(() => import('./pages/Consult/TestPage/TestPage'));

// misc
const Profile = React.lazy(() => import('./pages/Profile/Profile'))

const routes = [
    { path: '/consult', exact: true, name: 'Test Page', component: TestPage },

    { path: '/profile', exact: true, name: 'Profile', component: Profile }
];

export default routes;